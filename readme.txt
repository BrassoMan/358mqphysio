PUSH Arm Exercises Website Handover Document

The presumption behind the design of this site is such that it will be integrated with the Macquarie website. The site is a a handful of simple static webpages with CSS styling in the .html files to make integration easier. The HTML headers will need to be removed as necessary.

There is an extas folder not currently used which can be discarded if desired. It contains some banners which we decided not to use; and the old .css file.


Structure of files:

Repo
+---extras
�   +---banners
+---media
�   +---buttons
+---videopages